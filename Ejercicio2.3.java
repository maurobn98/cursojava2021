package modulo2;

public class Ejercicio3 {

	public static void main(String[] args) {
		char tipo='a'; //usamos este tipo de variable para poder poner una letra
		byte goles=3; //usamos byte porque tiene un valor m�ximo de 127, siendo el tipo de variable
		//con menos bits posible y con valor m�ximo mucho menor al n�mero de goles posible
		int capacidad=70000; //uso int porque el short se me quedar�a corto para capacidades como la que se pide
		float promedio=3.3f; //necesitamos usar float porque al hacer un promedio pueden aparecer decimales
		
		System.out.println("Tipo de divisi�n = "+tipo);
		System.out.println("N�mero de goles = "+goles);
		System.out.println("Capacidade do estadio = "+capacidad);
		System.out.println("Promedio de goles = "+promedio);
		
	}

}
