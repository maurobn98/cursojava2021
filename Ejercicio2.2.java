package modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
		byte bmin=-128;
		byte bmax=127;
		short smin=-32768;
		short smax=32767;
		int imin=-2147483648;
		int imax=2147483647;
		long lmin=-9223372036854775808l;
		long lmax=9223372036854775807l;
		System.out.println("Tipos de dato\tm�nimo\tm�ximo");
		System.out.println("----\t------\t------");
		System.out.println("\nbyte\t"+bmin+"\t"+bmax);
		System.out.println("\nshort\t"+smin+"\t"+smax);
		System.out.println("\nint\t"+imin+"\t"+imax);
		System.out.println("\nlong\t"+lmin+"\t"+lmax);
	}
}
	//Para calcular el rango m�ximo utilizamos la f�rmula 2^(n-1)-1
	//Y para el rango m�nimo usamos -2^(n-1)
	//Siendo n el n�umero de bits que conforman la variable
	//NOTA: Uso el s�mbolo ^para denotar potencia, aunque no sea as� en Java
