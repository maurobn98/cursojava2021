package pr�ctica4;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		System.out.println("Introduce tu posici�n: ");
		Scanner sc = new Scanner(System.in);
		byte posicion=sc.nextByte();
		
		if(posicion==1)
			System.out.println("�Obtienes la medalla de oro!");
		else if(posicion==2)
			System.out.println("�Obtienes la medalla de plata!");
		else if(posicion==3)
			System.out.println("�Obtienes la medalla de bronce!");
		else
			System.out.println("Sigue participando.");
	}

}
