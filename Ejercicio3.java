package cursojava;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Esbozo de método generado automáticamente
		System.out.println("Tecla de escape\t\tsignificado");
		System.out.println("\n\\n\t\t\tsignifica nueva línea");
		System.out.println("\\t\t\t\tsignifica un tab de espacio");
		System.out.println("\\\"\t\t\tes para poner \"(comillas dobles) dentro del texto por ejemplo \"Belencita\")");
		System.out.println("\\\t\t\tse utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\");
		System.out.println("\'\t\t\tse utiliza para las \'(comilla simple) para escribir por ejemplo \'Princesita\'");
	}

}
