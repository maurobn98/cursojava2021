package pr�ctica4;

import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {
		System.out.println("Introduce tu curso: ");
		Scanner sc = new Scanner(System.in);
		int curso=sc.nextInt();
		
		if(curso==0)
			System.out.println("Jardin de infantes");
		else if(curso>=1 && curso<=6)
			System.out.println("Primaria");
		else if(curso>=7 && curso<=12)
			System.out.println("Secundaria");
		else
			System.out.println("Nada");
	}

}
